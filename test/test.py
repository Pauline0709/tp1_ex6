#!/usr/bin/env python
# coding: utf_8

from calculator.simplecalculator import SimpleCalculator

"""
Module de test
"""

import unittest


class AdditionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_addition_entier_caracter(self):
        result = self.calculator.sum(1, "2")
        self.assertEqual(result, "ERROR")


class SubtractionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_subtraction_entier_caractere(self):
        result = self.calculator.subtract(1, "2")
        self.assertEqual(result, "ERROR")


class MultiplicationTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_multiplication_entier_caractere(self):
        result = self.calculator.subtract(1, "2")
        self.assertEqual(result, "ERROR")


class DivisionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_division_entier_caractere(self):
        result = self.calculator.divide(1, "2")
        self.assertEqual(result, "ERROR")


if __name__ == "__main__":
    unittest.main()
